# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Post.create(title: 'first shot of espresso', body: 'I took a shot of espresso and it was awesome!')
Post.create(title: 'Double Espresso', body: 'I took two shots of espresso and it was fantastic!')
User.create(username: "pixie", email: "pixie2020@gmail.com")
User.create(username: "luei", email: "foodfood4ever@gmail.com")