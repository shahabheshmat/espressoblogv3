class Post < ApplicationRecord
    belongs_to :user
    validates :user_id, presence: true
    validates :title, presence: true, length: { minimum: 3, maximum: 100}
    validates :body, presence: true, length: {minimum: 10, maximum: 2000}

end
