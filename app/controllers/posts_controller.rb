class PostsController < ApplicationController
    before_action :find_post, only: [:edit, :update, :show, :destroy]
    before_action :authenticate_user!, except: [:index, :show]

    def new
        #    @post = Post.new
         @post = current_user.posts.build
    end

    def index  
        @posts = Post.order("posts.created_at DESC").paginate(page: params[:page], per_page: 5)
    end

    def edit
    end

    def update
        if @post.update(post_params)
            flash[:success] = "Post successfuly Updated"
            redirect_to post_path(@post)
        else
            flash[:success] = "Post was NOT updated"
            render "edit"
        end
    end

    def create
        @post = current_user.posts.build(post_params)
        @post.user = User.first
        if @post.save
            flash[:success] = "Your Post was successfully Created!"
            redirect_to post_path(@post)
        else
            render "new"
        end    
    end

    def show
    end

    def destroy
        @post.destroy
        flash[:success] = "Post was successfully deleted"
        redirect_to posts_path
    end
    
    private
    def post_params
        params.require(:post).permit(:title, :body)
    end

    def find_post
        @post = Post.find(params[:id])  
    end
end
